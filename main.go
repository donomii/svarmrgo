// Support functions for svarmr
package svarmrgo

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/exec"
)

var AppDir, SvarmrDir string
var debugMode bool

// To hold incoming messages before processing
type message struct {
	port net.Conn
	raw  string
}

// Svarmr message struct
type Message struct {
	Conn      net.Conn
	Selector  string
	Arg       string
	Args      []string
	NamedArgs map[string]string
}

// Subprocess wrapper
type SubProx struct {
	In  io.WriteCloser
	Out io.ReadCloser
	Err io.ReadCloser
	Cmd *exec.Cmd
}

type MessageHandler func(net.Conn, Message)
type MessageHandler2 func(Message) []Message

var connList []net.Conn

func Debug(s string) {
	if debugMode {
		log.Println(s)
	}
}

func debug(s string) {
	if debugMode {
		log.Println(s)
	}
}

func debugf(s string, args ...interface{}) {
	if debugMode {
		log.Printf(s, args...)
	}
}

func HandleConnection(conn net.Conn, Q chan message) {
	scanner := bufio.NewScanner(conn)
	for {
		debug("Outer scanner loop")
		for scanner.Scan() {
			debug("Inner scanner loop")
			var m message = message{conn, scanner.Text()}
			Q <- m
		}
	}

}

// Read command line options
func CliConnect() net.Conn {
	AppDir = "./"
	SvarmrDir = "./"
	flag.StringVar(&AppDir, "appdir", "./", "Full path to applicaton directory")
	flag.StringVar(&SvarmrDir, "svarmrdir", "./", "Full path to svarmr directory")
	flag.BoolVar(&debugMode, "debug", false, "Enable debug output")
	flag.Parse()
	return nil //Find some way to shutdown svarmrgo without using global variables
}

// Connect to a svarmr server on host:port
func ConnectHub(server, port string) net.Conn {
	conn, err := net.Dial("tcp", server+":"+port)
	if err != nil {
		log.Printf("\nCould not connect to hub because: %v\n\n", err)
		os.Exit(1)
	}
	return conn
}

// Prepares the wire format version of the message, suitable for printing or sending
func WireFormat(m Message) string {
	out, _ := json.Marshal(m)
	return fmt.Sprintf("%s\n", out)
}

// Build a response to a message.  Messages will soon contain unique ID numbers, allowing responses to be matched to messages
func (m *Message) Response(response Message) string {
	return WireFormat(response)
}

// Respond to a message.  Messages will soon contain unique ID numbers, allowing responses to be matched to messages
//
// Always replies on the same port we received the message from
func (m *Message) Respond(response Message) {
	out := response.Response(response)
	if m.Conn == nil {
		fmt.Fprintf(os.Stdout, "%s", out)
	} else {
		fmt.Fprintf(m.Conn, "%s", out)
	}
}

// Send a message.  Messages will soon contain unique ID numbers, allowing responses to be matched to messages
//
// If conn is nil, then it will use whatever port the message was received on, the same as RespondMessage
func SendMessage(conn net.Conn, m Message) {
	o, _ := json.Marshal(m)
	out := fmt.Sprintf("%s\n", o)
	if conn == nil {
		conn = m.Conn
	}
	if conn == nil {
		fmt.Fprintf(os.Stdout, "%s", out)
	} else {
		fmt.Fprintf(conn, "%s", out)
	}
}

// Send a simple message with a selector and a scalar argument
func SimpleSend(conn net.Conn, selector, arg string) {
	SendMessage(conn, Message{conn, selector, arg, []string{}, map[string]string{}})
}

// Handle incoming messages.  This will read a message, unpack the JSON, and call the MessageHandler with the unpacked message
//
// MessageHandler must look like:
//
//	func handleMessage (m svarmrgo.Message) []svarmrgo.Message
//
// We call handleMessage with a message, and it returns an array of svarmrgo.Message
// We then send all the returned messages
func HandleInputLoop(conn net.Conn, callback MessageHandler2) {
	defer func() {
		if r := recover(); r != nil {
			log.Println("Error handling incoming messages: ", r)
		}
	}()

	var r *bufio.Reader
	if conn != nil {
		r = bufio.NewReader(conn)

		for {
			debug("Outer handle inputs loop")
			l, err := r.ReadString('\n')
			if err != nil {
				log.Println(err)
				os.Exit(1)
			}
			if l != "" {
				var text = l
				if len(text) > 10 {
					var m Message
					err := json.Unmarshal([]byte(text), &m)
					if err != nil {
						log.Println("error decoding message!:", err, text)
					} else {
						m.Conn = conn
						messages := callback(m)
						for _, message := range messages {
							SendMessage(m.Conn, message)
						}
					}
				} else {
					debugf("Invalid message: '%v'\n", text)
				}
			} else {
				debug("Empty message received\n")
			}
		}
	} else {

		log.Println("Using pipes")
		r = bufio.NewReader(os.Stdin)
		for {
			l, err := r.ReadString('\n')
			if err != nil {
				log.Println(err)
				os.Exit(1)
			}
			if l != "" {
				var text = l
				if len(text) > 10 {
					var m Message
					err := json.Unmarshal([]byte(text), &m)
					if err != nil {
						log.Println("error decoding message!:", err, text)
					} else {
						m.Conn = conn
						messages := callback(m)
						for _, message := range messages {
							SendMessage(m.Conn, message)
						}
					}
				} else {
					debugf("Invalid message: '%v'\n", text)
				}
			} else {
				debug("Empty message received\n")
			}
		}

	}
}
